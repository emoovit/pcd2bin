# pcd2bin


Convert .pcd files into .bin files


Start with converting pointclouds from .bag or sensor_msgs/pointcloud2 into .pcd files, following this page http://wiki.ros.org/pcl_ros 


Then, run this script to convert all .pcd files into .bin files. Place the appropriate directories inside the script. 
