#!/usr/bin/env python

# .pcd to .bin file converter
# 
# .pcd files are text files, while .bin files are binary 
# .pcd files starts with a header, then the data points
# with x, y, z, intensity and ring values
# .bin files are just the data with x, y, z, and intensity.
# So to convert from .pcd to .bin, just remove the header,
# then copy the data x, y, z, intensity. 
# But to copy, need to convert the number into its 
# binary representation. Since all these numbers are floats, 
# they will each take 4 bytes. 

import struct
import os

pcd_dir = "/home/umar/pcd/pcd_files"
bin_dir = "/home/umar/pcd/bin_files"

def float_hex4(f):
    # convert float into its 4 bytes representation 
    ba = bytearray(struct.pack("f", f))  
    return ba 

for filename in os.listdir(pcd_dir):
    arr_bin = []
    data_start = False 

    in_file = pcd_dir + "/" + filename
    print("Converting ") + in_file
    # Open the .pcd file
    with open(in_file, "r") as text_file:
        for line in text_file:
            # print line
            # loop through until we find the final line of the header
            if data_start == False and line == "DATA ascii\n":
                data_start = True 
            # then start reading the values, convert to binary 
            # and store in an array
            elif data_start:
                arr_txt = line.split(" ") # this automatically removes the final 'ring' value
                for val in arr_txt:
                    arr_bin.append(float_hex4(float(val)))

        # Next just write the values into a .bin file
        out_file = bin_dir + "/" + filename[:-3] + "bin"
        with open(out_file, "wb") as binary_file:
            for val in arr_bin:
                binary_file.write(val)

    print("> Done " + out_file)